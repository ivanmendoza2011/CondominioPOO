/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class Factura {
    private int numfac;
    private int contrato;
    private String detalle;
    private float monto;
    private float balance;

    public int getCodserv() {
        return codserv;
    }

    public void setCodserv(int codserv) {
        this.codserv = codserv;
    }
    private int codserv;

    public Factura() {
        
    }

    public Factura(int numfac, int contrato, String detalle, float monto, float balance) {
        this.numfac = numfac;
        this.contrato = contrato;
        this.detalle = detalle;
        this.monto = monto;
        this.balance = balance;
    }

    public int getNumfac() {
        return numfac;
    }

    public void setNumfac(int numfac) {
        this.numfac = numfac;
    }

    public int getContrato() {
        return contrato;
    }

    public void setContrato(int contrato) {
        this.contrato = contrato;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Factura{" + "numfac=" + numfac + ", contrato=" + contrato + ", detalle=" + detalle + ", monto=" + monto + ", balance=" + balance + '}';
    }

    public int InsertarFactura(){
        
        String cmd ="exec FacturaActualiza '"+this.numfac+"','"+this.contrato+"','','0','"+this.detalle+"','"+this.monto+"','"+this.balance+"'";
        boolean rs=conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        
        if (rs == true) {
            JOptionPane.showMessageDialog(null, "Error No Hay datos");
            return 0;
        }
        return 1;
    }
    public int InsertarDetalle(){
                
        String cmd ="exec Detalle_FacturaActualiza '"+this.numfac+"','"+this.codserv+"','"+this.monto+"','0','0','1'";

        boolean rs=conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        
        if (rs == true) {
            JOptionPane.showMessageDialog(null, "Error No Hay datos");
            return 0;
        }
        return 1;
    }
    
    public ResultSet BuscarFactura(String nomcli,String fecini,String fecfin){
        return conexion.ConectarBD.ejecuta("select f.numfac,f.contrato,f.fecha,cl.nomcli,f.monto,f.balance from tb_factura f inner join tb_contrato c on c.codcon=f.contrato inner join tb_clientes cl on c.codcli=cl.codcli where cl.nomcli like ('%"+nomcli+"%') and fecha>='"+fecini+"' and fecha<='"+fecfin+" 23:59:59'");
    }
}
