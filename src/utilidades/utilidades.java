/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import com.toedter.calendar.JDateChooser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.swing.JComboBox;

/**
 *
 * @author Ivan
 */
public class utilidades {

    public static void llenar_jcombobox(JComboBox jcmb, String cmd, String Campo) throws SQLException {

        jcmb.removeAllItems();
        ResultSet rs = conexion.ConectarBD.ejecuta(cmd);

        while (rs.next()) {
            jcmb.addItem(rs.getString(Campo));
        }

        if (jcmb.getSelectedItem() == "") {
            jcmb.addItem("No Hay más.");
            jcmb.setEnabled(false);
        }
    }

    public static String FechaSQL(JDateChooser valor) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String fecha = format.format(valor.getDate());
        return fecha;
    }
}
