/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class Clientes {

    private int codcli;
    private String nomcli;
    private String telcli;
    private String corcli;
    private boolean estcli;
    private String dircli;

    public Clientes() {
    }

    public Clientes(int codcli) {
        this.codcli = codcli;
    }

    public Clientes(int codcli, String nomcli, String telcli, String corcli, boolean estcli, String dircli) {
        this.codcli = codcli;
        this.nomcli = nomcli;
        this.telcli = telcli;
        this.corcli = corcli;
        this.estcli = estcli;
        this.dircli = dircli;
    }

    public int getCodcli() {
        return codcli;
    }

    public void setCodcli(int codcli) {
        this.codcli = codcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getTelcli() {
        return telcli;
    }

    public void setTelcli(String telcli) {
        this.telcli = telcli;
    }

    public String getCorcli() {
        return corcli;
    }

    public void setCorcli(String corcli) {
        this.corcli = corcli;
    }

    public boolean isEstcli() {
        return estcli;
    }

    public void setEstcli(boolean estcli) {
        this.estcli = estcli;
    }

    public String getDircli() {
        return dircli;
    }

    public void setDircli(String dircli) {
        this.dircli = dircli;
    }

    @Override
    public String toString() {
        return "Clientes{" + "codcli=" + codcli + ", nomcli=" + nomcli + ", telcli=" + telcli + ", corcli=" + corcli + ", estcli=" + estcli + ", dircli=" + dircli + '}';
    }

    public int insertar_actualizaCliente() {
        String cmd = "exec ClientesActualiza '"+this.codcli+"','"+this.nomcli+"','"+this.telcli+"','"+this.corcli+"','"+this.estcli+"','"+this.dircli+"'";
        boolean rs = conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        if (rs == true) {
            JOptionPane.showMessageDialog(null, "Error No Hay datos");
            return 0;
        }
        return 1;
    }
    
    public ResultSet buscar_clientes_id(int id,String desc,int est){
        String cmd="select * from tb_clientes where ("+est+"=0 or estcli=1)";
        
        if(id!=0){
            cmd+=" and codcli="+String.valueOf(id);
        }
        
        if(desc.length()>0){
            cmd+=" and nomcli like ('%"+desc+"%')";
        }
        
        return conexion.ConectarBD.ejecuta(cmd);
    }
    
    public ResultSet buscar_clientes(){
        return conexion.ConectarBD.ejecuta("select * from tb_clientes");
    }
}
