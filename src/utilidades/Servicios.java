/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class Servicios {

    private int codser;
    private String desser;
    private boolean estser;
    private float monser;
    private float recser;
    private int diaser;

    public Servicios() {
    }

    public Servicios(int codser) {
        this.codser = codser;
    }

    public Servicios(int codser, String desser, boolean estser, float monser, float recser, int diaser) {
        this.codser = codser;
        this.desser = desser;
        this.estser = estser;
        this.monser = monser;
        this.recser = recser;
        this.diaser = diaser;
    }

    public int getCodser() {
        return codser;
    }

    public void setCodser(int codser) {
        this.codser = codser;
    }

    public String getDesser() {
        return desser;
    }

    public void setDesser(String desser) {
        this.desser = desser;
    }

    public boolean isEstser() {
        return estser;
    }

    public void setEstser(boolean estser) {
        this.estser = estser;
    }

    public float getMonser() {
        return monser;
    }

    public void setMonser(float monser) {
        this.monser = monser;
    }

    public float getRecser() {
        return recser;
    }

    public void setRecser(float recser) {
        this.recser = recser;
    }

    public int getDiaser() {
        return diaser;
    }

    public void setDiaser(int diaser) {
        this.diaser = diaser;
    }

    @Override
    public String toString() {
        return "Servicios{" + "codser=" + codser + ", desser=" + desser + ", estser=" + estser + ", monser=" + monser + ", recser=" + recser + ", diaser=" + diaser + '}';
    }

    public int insertar_actualizaServicio() {

        String cmd = "exec ActualizaServicios '"+this.codser+"','"+this.desser+"','"+this.estser+"','"+this.monser+"','"+this.recser+"','"+this.diaser+"'";
        
        boolean rs = conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        if (rs == true) {
            JOptionPane.showMessageDialog(null, "Error No Hay datos");
            return 0;
        }
        return 1;
    }
    
    public ResultSet buscar_servicios_id(int id,String desc,int est){
        String cmd="select * from tb_servicios where ("+est+"=0 or estser=1)";
        
        if(id!=0){
            cmd+=" and codser="+String.valueOf(id);
        }
        
        if(desc.length()>0){
            cmd+="and desser like ('%"+desc+"%')";
        }
        
        return conexion.ConectarBD.ejecuta(cmd);
    }

}
